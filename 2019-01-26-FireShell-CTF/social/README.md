# Social

The flag was encoded in the QR code visible on the picture on the FireShell's Facebook page.
![](poster_with_qr.jpg)
Decoding it gives:
```
https://fireshellsecurity.team/

https://www.facebook.com/fireshellst/

https://twitter.com/fireshellst

F#{4re_Y0u_r3c0n?!!_:)}
```
The flag is `F#{4re_Y0u_r3c0n?!!_:)}`.