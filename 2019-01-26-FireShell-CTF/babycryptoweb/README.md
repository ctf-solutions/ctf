# babycryptoweb

We get the following code at the challenge's web site:
```php
<?php

$code = '$kkk=5;$s="e1iwZaNolJeuqWiUp6pmo2iZlKKulJqjmKeupalmnmWjVrI=";$s=base64_decode($s);$res="";for($i=0,$j=strlen($s);$i<$j;$i++){$ch=substr($s,$i,1);$kch=substr($kkk,($i%strlen($kkk))-1,1);$ch=chr(ord($ch)+ord($kch));$res.=$ch;};echo $res;';
    
if (isset($_GET['p']) && isset($_GET['b']) && strlen($_GET['b']) === 1 && is_numeric($_GET['p']) && (int) $_GET['p'] < strlen($code)) {
    $p = (int) $_GET['p'];
    $code[$p] = $_GET['b'];
    eval($code);
} else {
    show_source(__FILE__);
}

?>
```
We must change one character in the `$code` to make it return the flag. Let's tidy the code up:
```php
<?php
 $kkk=5;
 $s="e1iwZaNolJeuqWiUp6pmo2iZlKKulJqjmKeupalmnmWjVrI=";
 $s=base64_decode($s);
 $res="";
 for($i=0,$j=strlen($s);$i<$j;$i++){
   $ch=substr($s,$i,1); # char at position $i
   $kch=substr($kkk,($i%strlen($kkk))-1,1); # Value of $kkk
   $ch=chr(ord($ch)+ord($kch));
   $res.=$ch;
 };
 echo $res;
```
The code base64-decodes `$s` and then increments its every character code by the ASCII code of `$kkk` value (`'5'`) - `53`. Base64-decoded `$s` is `{X°e£h®©h§ªf£h¢®£§®¥©fe£V²`. What is important is the first character - it's `{` (ASCII code `123`). The flag should be `F{xxx}`, so the first character should be `F` (ASCII code `70`). The original code increases the ASCII code of each character by `53`, but what we need is to decrease it by `53`. The solution is to substitute `+` with `-` in `$ch=chr(ord($ch)+ord($kch));` and execute the code.

The flag is `F#{0n3_byt3_ru1n3d_my_encrypt1i0n!}`.