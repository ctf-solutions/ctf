# FireShell CTF 2019

https://ctf.fireshellsecurity.team/

## Solutions
* [Bad injections](bad_injections)
* [Vice](vice)
* [Python Learning Environment](python_learning_environment)
* [babycryptoweb](babycryptoweb)
* [Social](social)